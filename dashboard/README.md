# Dashboard

IoT WEB Dashboard for ESP32 Data

## Build

    docker build -t salustioal/dashboard .

## Run

    docker run -d -p 80:8050 --name dashboard --link rest-server salustioal/dashboard

